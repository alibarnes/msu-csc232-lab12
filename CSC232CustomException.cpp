/*
 * CSC232CustomException.cpp
 *
 *  Created on: Dec 5, 2016
 *      Author: jdaehn
 */

#include "CSC232CustomException.h"

CSC232CustomException::CSC232CustomException(const std::string& message) :
		std::logic_error(message) {
	// no-op; delegate to std::logic_error
}
