#include "NotFoundException.h"

NotFoundException::NotFoundException(const std::string& message) :
		CSC232CustomException("Not found exception: " + message) {
	// no-op; delegate to CSC232CustomException
}
